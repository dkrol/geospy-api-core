@echo off

cd C:\Users\kdare\Desktop\DK\workspace\gitlab\GeoSpy\geospy-api-core\GS.Api\src\GS.Api

echo --------------------------
echo - dotnet restore
echo --------------------------

dotnet restore

echo --------------------------
echo - dotnet build
echo --------------------------

dotnet build

echo --------------------------
echo - dotnet publish
echo --------------------------

dotnet publish

echo --------------------------
echo - remove publish folder
echo --------------------------

cd C:\Users\kdare\Desktop\DK\workspace\gitlab\GeoSpy\geospy-api-core\GS.Publish
RD /S /Q publish

echo --------------------------
echo - move publish folder
echo --------------------------

cd C:\Users\kdare\Desktop\DK\workspace\gitlab\GeoSpy\geospy-api-core\GS.Api\src\GS.Api\bin\Debug\netcoreapp1.0
move publish C:\Users\kdare\Desktop\DK\workspace\gitlab\GeoSpy\geospy-api-core\GS.Publish

PAUSE