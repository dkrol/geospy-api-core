// MONGODB

mongodump -d <database_name> -o <directory_backup>
mongorestore -d <database_name> <directory_backup>

// Kompilacja DOTNET CORE API !!!

1. Zmiana w pliku project.json (Aplikacja GS.Api/src) (Przed publikacją aplikacji) z lini komand
- Dodanie w miejscu dependencies: { ... 
-----------------------------------------

"System.ComponentModel.TypeConverter": {
  "version": "4.3.0",
  "exclude": "runtime"
},
"System.ComponentModel.Primitives": {
  "version": "4.3.0",
  "exclude": "runtime"
},
"System.Collections.Specialized": {
  "version": "4.3.0",
  "exclude": "runtime"
},
"System.Linq": {
  "version": "4.3.0",
  "exclude": "runtime"
},
"System.Threading": {
  "version": "4.3.0",
  "exclude": "runtime"
},
"System.Collections.NonGeneric": {
  "version": "4.3.0",
  "exclude": "runtime"
},

-----------------------------------------
2. Z lini komand
	dotnet restore
	dotnet build
	dotnet publish

3. Kopiowanie paczki publish
4. Zmiana w pliku *.runtimeconfig.json (już w paczce opublikowanej)
-----------------------------------------
"version": "1.2.0-beta-001291-00"
-----------------------------------------