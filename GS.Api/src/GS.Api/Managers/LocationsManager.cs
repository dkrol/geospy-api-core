﻿using GS.Api.Common;
using GS.Command.Command;
using GS.Command.Locations;
using GS.Query.GetLocations;
using GS.Query.Query;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace GS.Api.Managers
{
    public class LocationsManager : BaseManager
    {
        public IActionResult GetLocations()
        {
            var queryBus = new QueryBus(new GetLocationsHandler());
            var query = queryBus.SendQuery(new GetLocationsQuery());

            return Ok(query.Locations);
        }

        public IActionResult GetLocationsByDeviceId(string deviceId)
        {
            var queryBus = new QueryBus(new GetLocationsByDeviceIdHandler());
            var query = new GetLocationsByDeviceIdQuery()
            {
                DeviceId = deviceId
            };

            queryBus.SendQuery(query);
            return Ok(query.Locations);
        }

        public IActionResult GetLastLocationByDeviceId(string deviceId)
        {
            var queryBus = new QueryBus(new GetLastLocationByDeviceIdHandler());
            var query = new GetLastLocationByDeviceIdQuery
            {
                DeviceId = deviceId
            };

            queryBus.SendQuery(query);
            return Ok(query.Location);
        }

        public IActionResult AddLocation(AddLocationCommand locationCommand, ModelStateDictionary modelState)
        {
            var commandBus = new CommandBus(new AddLocationHandler());

            if (locationCommand != null && modelState.IsValid)
            {
                var result = commandBus.SendCommand(locationCommand);

                if (!result.IsValid && !string.IsNullOrEmpty(result.Message))
                {
                    return BadRequest(ParseHelper.GetModelStateErrorFromString(result.Message));
                }

                return Ok(result);
            }
            else
            {
                return BadRequest(ParseHelper.GetModelStateErrors(modelState));
            }
        }

        public IActionResult GetLocationsByDeviceIdAndTimestampRange(string deviceId, long from, long to)
        {
            var queryBus = new QueryBus(new GetLocationsByDeviceIdAndTimestampRangeHandler());
            var query = new GetLocationsByDeviceIdAndTimestampRangeQuery()
            {
                DeviceId = deviceId,
                From = from,
                To = to
            };

            queryBus.SendQuery(query);
            return Ok(query.Locations);
        }
    }
}
