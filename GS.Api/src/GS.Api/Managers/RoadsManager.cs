﻿using GS.Domain.Road;
using GS.Query.GetRoads;
using GS.Query.Query;
using Microsoft.AspNetCore.Mvc;

namespace GS.Api.Managers
{
    public class RoadsManager : BaseManager
    {
        public IActionResult GetRoadsForDayByDeviceId(string deviceId, long date)
        {
            var queryBus = new QueryBus(new GetRoadsForDayByDeviceIdHandler());

            var road = new Road()
            {
                DeviceId = deviceId,
                Date = date
            };

            var query = new GetRoadsForDayByDeviceIdQuery()
            {
                DeviceId = deviceId,
                Date = date,
                Road = road
            };

            var result = queryBus.SendQuery(query);
            return Ok(result.Road);
        }
    }
}
