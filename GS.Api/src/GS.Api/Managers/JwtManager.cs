﻿using GS.Api.Common;
using GS.Domain.Role;
using GS.Domain.User;
using GS.Query.GetUsers;
using GS.Query.Query;

namespace GS.Api.Managers
{
    public class JwtManager
    {
        public bool IsUserExist(User user, out string userRole)
        {
            bool result = false;
            userRole = null;

            QueryBus queryBus = new QueryBus(new GetUserByNameHandler());
            GetUserByNameQuery query = new GetUserByNameQuery()
            {
                UserName = user.UserName
            };

            GetUserByNameQuery queryResult = queryBus.SendQuery(query);

            if (queryResult.User != null)
            {
                user.Password = HashHelper.GetMd5HashWithSale(user.Password, queryResult.User.Sale);
                result = user.Password == queryResult.User.Password;
                userRole = queryResult.UserResponse.Role.Name.ToString();
            }

            return result;
        }
    }
}
