﻿using System;
using GS.Api.Common;
using GS.Command.Command;
using GS.Command.Users;
using GS.Query.GetUsers;
using GS.Query.Query;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace GS.Api.Managers
{
    public partial class UsersManager : BaseManager
    {
        public IActionResult GetUsers()
        {
            QueryBus queryBus = new QueryBus(new GetUsersHandler());
            GetUsersQuery query = queryBus.SendQuery(new GetUsersQuery());

            return Ok(query.UsersResponse);
        }

        public IActionResult GetUserById(string userId)
        {
            QueryBus queryBus = new QueryBus(new GetUserByIdHandler());
            GetUserByIdQuery query = new GetUserByIdQuery { UserId = userId };

            queryBus.SendQuery(query);
            return Ok(query.UserResponse);
        }

        public IActionResult AddUser(AddUserCommand userCommand, ModelStateDictionary modelState)
        {
            CommandBus commnadBus = new CommandBus(new AddUserHandler());

            if (userCommand != null && modelState.IsValid)
            {
                userCommand.Sale = GeneratorHelper.GenerateSalt();
                userCommand.Password = HashHelper.GetMd5HashWithSale(userCommand.Password, userCommand.Sale);
                var result = commnadBus.SendCommand(userCommand);

                if (!result.IsValid && !String.IsNullOrEmpty(result.Message))
                {
                    return BadRequest(ParseHelper.GetModelStateErrorFromString(result.Message));
                }

                return Ok(result);
            }
            else
            {
                return BadRequest(ParseHelper.GetModelStateErrors(modelState));
            }
        }

        public IActionResult UpdateUser(UpdateUserCommand userCommand)
        {
            CommandBus commandBus = new CommandBus(new UpdateUserHandler());
            var result = commandBus.SendCommand(userCommand);
            return Ok(result);
        }

        public void DeleteUserById(string userId)
        {
            CommandBus commandBus = new CommandBus(new DeleteUserByIdHandler());
            DeleteUserByIdCommand command = new DeleteUserByIdCommand() { UserId = userId };
            commandBus.SendCommand(command);
        }
    }
}
