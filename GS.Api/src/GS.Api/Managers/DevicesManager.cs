﻿using System;
using GS.Api.Common;
using GS.Command.Command;
using GS.Command.Devices;
using GS.Query.GetDevices;
using GS.Query.Query;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Logging;

namespace GS.Api.Managers
{
    public class DevicesManager : BaseManager
    {
        public IActionResult GetDevices()
        {
            QueryBus queryBus = new QueryBus(new GetDevicesHandler());
            GetDevicesQuery query = queryBus.SendQuery(new GetDevicesQuery());

            return Ok(query.Devices);
        }

        public IActionResult GetDevicesByUserName(string userName)
        {
            QueryBus queryBus = new QueryBus(new GetDevicesByUserNameHandler());
            GetDevicesByUserNameQuery query = new GetDevicesByUserNameQuery()
            {
                UserName = userName
            };

            queryBus.SendQuery(query);
            return Ok(query.Devices);
        }

        public IActionResult GetDeviceById(string deviceId)
        {
            QueryBus queryBus = new QueryBus(new GetDeviceByIdHandler());
            GetDeviceByIdQuery query = new GetDeviceByIdQuery { Id = deviceId };

            queryBus.SendQuery(query);
            return Ok(query.Device);
        }

        public IActionResult AddDevice(AddDeviceCommand deviceCommand, ModelStateDictionary modelState)
        {
            CommandBus commnadBus = new CommandBus(new AddDeviceHandler());

            if (deviceCommand != null && modelState.IsValid)
            {
                var result = commnadBus.SendCommand(deviceCommand);

                if (result.IsValid && !String.IsNullOrEmpty(result.Message))
                {
                    return BadRequest(ParseHelper.GetModelStateErrorFromString(result.Message));
                }

                return Ok(result);
            }
            else
            {
                return BadRequest(ParseHelper.GetModelStateErrors(modelState));
            }
        }

        public IActionResult UpdateDevice(UpdateDeviceCommand updateDeviceCommand)
        {
            CommandBus commandBus = new CommandBus(new UpdateDeviceHandler());
            var result = commandBus.SendCommand(updateDeviceCommand);

            if (result.IsValid && !String.IsNullOrEmpty(result.Message))
            {
                return BadRequest(ParseHelper.GetModelStateErrorFromString(result.Message));
            }

            return Ok(result);
        }
    }
}
