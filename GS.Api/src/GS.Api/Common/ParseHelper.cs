﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace GS.Api.Common
{
    public class ParseHelper
    {
        public static IEnumerable<ModelError> GetModelStateErrors(ModelStateDictionary modelState)
        {
            IEnumerable<ModelError> result = null;

            if (!modelState.IsValid)
            {
                result = modelState.Values.SelectMany(v => v.Errors);

            }

            return result;
        }

        public static IEnumerable<ModelError> GetModelStateErrorFromString(string messages)
        {
            List<ModelError> result = new List<ModelError>();
            var messagesArray = messages.Split(',');
            foreach (string msg in messagesArray)
            {
                result.Add(new ModelError(msg));
            }

            return result;
        }

    }
}
