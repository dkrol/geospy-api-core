﻿namespace GS.Api.Common
{
    public class JwtConst
    {
        public const string UserPolicy = "GeoSpyUser";
        public const string UserRole = "role";

        public const string USER_CLAIM_VALUE = "GeoSpyUserClaimValue";
    }
}
