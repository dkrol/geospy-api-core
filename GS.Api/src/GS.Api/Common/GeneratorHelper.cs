﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GS.Api.Common
{
    public class GeneratorHelper
    {
        public static string GenerateSalt()
        {
            return new Random().Next(1000, 9999).ToString();
        }
    }
}
