﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace GS.Api.Common
{
    public class HashHelper
    {
        public static string GetMd5Hash(string input)
        {
            MD5 md5 = MD5.Create();
            byte[] inputBytes = Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }

            return sb.ToString();
        }

        public static bool VerifyMd5Hash(string input, string hash)
        {
            var hashOfInput = GetMd5Hash(input);
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (comparer.Compare(hashOfInput, hash) == 0)
            {
                return true;
            }

            return false;
        }

        public static string GetMd5HashWithSale(string input, string sale)
        {
            var hash = GetMd5Hash(input);
            return GetMd5Hash(hash + sale);
        }

        public static bool VerifyMd5HashWithSale(string input, string hash, string sale)
        {
            var hashWithSale = GetMd5HashWithSale(input, sale);
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;
            if (comparer.Compare(hashWithSale, hash) == 0)
            {
                return true;
            }

            return false;
        }
    }
}
