﻿using GS.Api.Managers;
using GS.Command.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GS.Api.Controllers
{
    [Route("api/[controller]")]
    [AllowAnonymous]
    public class UsersController : Controller
    {
        private readonly UsersManager _usersManager = new UsersManager();

        [HttpGet]
        public IActionResult Get()
        {
            return _usersManager.GetUsers();
        }

        [HttpGet("{id}")]
        public IActionResult GetUserById(string id)
        {
            return _usersManager.GetUserById(id);
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult Post([FromBody]AddUserCommand userCommand)
        {
            return _usersManager.AddUser(userCommand, this.ModelState);
        }

        [HttpPut("{id}")]
        public IActionResult Put(string id, [FromBody]UpdateUserCommand userCommand)
        {
            userCommand.UserId = id;
            return _usersManager.UpdateUser(userCommand);
        }

        [HttpDelete("{id}")]
        public void Delete(string id)
        {
            _usersManager.DeleteUserById(id);
        }
    }
}
