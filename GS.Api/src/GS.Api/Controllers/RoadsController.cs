﻿using GS.Api.Managers;
using GS.Command.Locations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GS.Api.Controllers
{
    [Route("api/[controller]")]
    [AllowAnonymous]
    public class RoadsController : Controller
    {
        private readonly RoadsManager _roadsManager = new RoadsManager();

        [HttpGet("device/{id}/date/{date}")]
        public IActionResult GetRoadsForDayByDeviceId(string id, long date)
        {
            return _roadsManager.GetRoadsForDayByDeviceId(id, date);
        }
    }
}
