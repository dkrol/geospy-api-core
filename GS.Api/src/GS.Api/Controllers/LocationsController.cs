﻿using System.Collections.Generic;
using GS.Api.Managers;
using GS.Command.Locations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GS.Api.Controllers
{
    [Route("api/[controller]")]
    [AllowAnonymous]
    public class LocationsController : Controller
    {
        private readonly LocationsManager _locationsManager = new LocationsManager();

        [HttpGet]
        public IActionResult Get()
        {
            return _locationsManager.GetLocations();
        }

        [HttpGet("device/{id}")]
        public IActionResult GetLocationsByDeviceId(string id)
        {
            return _locationsManager.GetLocationsByDeviceId(id);
        }

        [HttpGet("device/{id}/last")]
        public IActionResult GetLastLocationByDeviceId(string id)
        {
            return _locationsManager.GetLastLocationByDeviceId(id);
        }

        [HttpGet("device/{id}/from/{from}/to/{to}")]
        public IActionResult GetLocationsByDeviceIdAndTimestampRange(string id, long from, long to)
        {
            return _locationsManager.GetLocationsByDeviceIdAndTimestampRange(id, from, to);
        }


        [HttpPost]
        public IActionResult Post([FromBody]AddLocationCommand locationCommand)
        {
            return _locationsManager.AddLocation(locationCommand, ModelState);
        }

        [HttpPost("list")]
        public IActionResult PostLocationsMany([FromBody]List<AddLocationCommand> locationCommandList)
        {
            foreach (var locationCommand in locationCommandList)
            {
                _locationsManager.AddLocation(locationCommand, ModelState);
            }

            return Ok();
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
