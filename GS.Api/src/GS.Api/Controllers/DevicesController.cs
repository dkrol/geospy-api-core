﻿using GS.Api.Managers;
using GS.Command.Devices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GS.Api.Controllers
{
    [Route("api/[controller]")]
    [AllowAnonymous]
    public class DevicesController : Controller
    {
        private readonly DevicesManager _devicesManager = new DevicesManager();

        [HttpGet]
        public IActionResult Get()
        {
            return _devicesManager.GetDevices();
        }

        [HttpGet("{id}")]
        public IActionResult GetDeviceById(string id)
        {
            return _devicesManager.GetDeviceById(id);
        }

        [HttpGet("user/{userName}")]
        public IActionResult GetDevicesByUserName(string userName)
        {
            return _devicesManager.GetDevicesByUserName(userName);
        }

        [HttpPost]
        public IActionResult Post([FromBody]AddDeviceCommand deviceCommand)
        {
            return _devicesManager.AddDevice(deviceCommand, ModelState);
        }

        [HttpPut("{id}")]
        public IActionResult Put(string id, [FromBody]UpdateDeviceCommand updateDeviceCommand)
        {
            updateDeviceCommand.Id = id;
            return _devicesManager.UpdateDevice(updateDeviceCommand);
        }


        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
