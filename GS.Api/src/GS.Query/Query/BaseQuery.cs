﻿namespace GS.Query.Query
{
    public class BaseQuery : IQuery
    {
        public bool IsValid { get; set; } = true;
        public string Message { get; set; }
    }
}
