﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GS.Query.Query
{
    interface IQueryBus
    {
        T SendQuery<T>(T query) where T : IQuery;
    }
}
