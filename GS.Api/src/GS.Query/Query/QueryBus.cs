﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GS.Query.HandleQuery;

namespace GS.Query.Query
{
    public class QueryBus : IQueryBus
    {
        private readonly IHandleQuery _queryFactory;
        public QueryBus(IHandleQuery queryFactory)
        {
            _queryFactory = queryFactory;
        }

        public T SendQuery<T>(T query) where T : IQuery
        {
            return ((IHandleQuery<T>) _queryFactory).Execute(query);
        }
    }
}
