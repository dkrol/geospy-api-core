﻿using System.Linq;
using GS.Db.DataBase;
using GS.Db.Factory;
using GS.Domain.Role;
using GS.Domain.User;
using GS.Query.HandleQuery;
using MongoDB.Driver;

namespace GS.Query.GetRoles
{
    public class GetRoleByUserNameHandler : IHandleQuery<GetRoleByUserNameQuery>
    {
        private readonly IMongoCollection<Role> _roleColl;
        private readonly IMongoCollection<User> _userColl;

        public GetRoleByUserNameHandler()
        {
            _userColl = MongoFactory.GetCollection<User>(DataConst.GsUser);
            _roleColl = MongoFactory.GetCollection<Role>(DataConst.GsRole);
        }

        public GetRoleByUserNameQuery Execute(GetRoleByUserNameQuery query)
        {
            var user = _userColl.Find<User>(usr => usr.UserName == query.UserName).FirstOrDefault();

            if (user != null)
            {
                query.Role = _roleColl.Find<Role>(role => role.UserId == user.Id).FirstOrDefault();
            }

            return query;
        }
    }
}
