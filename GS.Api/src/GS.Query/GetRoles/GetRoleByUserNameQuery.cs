﻿using GS.Domain.Role;
using GS.Query.Query;

namespace GS.Query.GetRoles
{
    public class GetRoleByUserNameQuery : IQuery
    {
        public string UserName { get; set; }
        public Role Role { get; set; }
    }
}
