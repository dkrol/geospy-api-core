﻿using System.Linq;
using GS.Domain.Device;
using GS.Query.Query;

namespace GS.Query.GetDevices
{
    public class GetDevicesQuery : IQuery
    {
        public IQueryable<Device> Devices { get; set; }
    }
}
