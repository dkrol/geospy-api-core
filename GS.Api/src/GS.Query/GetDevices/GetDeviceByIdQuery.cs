﻿using GS.Domain.Device;
using GS.Query.Query;

namespace GS.Query.GetDevices
{
    public class GetDeviceByIdQuery : IQuery

    {
        public string Id { get; set; }
        public Device Device { get; set; } 
    }
}
