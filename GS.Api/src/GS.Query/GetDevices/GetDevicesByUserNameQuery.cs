﻿using System.Linq;
using GS.Domain.Device;
using GS.Query.Query;

namespace GS.Query.GetDevices
{
    public class GetDevicesByUserNameQuery : IQuery
    {
        public string UserName { get; set; }
        public IQueryable<Device> Devices { get; set; }
    }
}
