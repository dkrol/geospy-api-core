﻿using GS.Db.DataBase;
using GS.Db.Factory;
using GS.Domain.Device;
using GS.Query.HandleQuery;
using MongoDB.Driver;

namespace GS.Query.GetDevices
{
    public class GetDeviceByIdHandler : IHandleQuery<GetDeviceByIdQuery>
    {
        private readonly IMongoCollection<Device> _deviceColl;

        public GetDeviceByIdHandler()
        {
            _deviceColl = MongoFactory.GetCollection<Device>(DataConst.GsDevice);
        }

        public GetDeviceByIdQuery Execute(GetDeviceByIdQuery query)
        {
            query.Device = _deviceColl.Find<Device>(dev => dev.Id == query.Id).FirstOrDefault();
            return query;
        }
    }
}
