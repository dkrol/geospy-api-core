﻿using GS.Db.DataBase;
using GS.Db.Factory;
using GS.Domain.Device;
using GS.Query.HandleQuery;
using MongoDB.Driver;

namespace GS.Query.GetDevices
{
    public class GetDevicesHandler : IHandleQuery<GetDevicesQuery>
    {
        private readonly IMongoCollection<Device> _deviceColl;

        public GetDevicesHandler()
        {
            _deviceColl = MongoFactory.GetCollection<Device>(DataConst.GsDevice);
        }

        public GetDevicesQuery Execute(GetDevicesQuery query)
        {
            query.Devices = _deviceColl.AsQueryable<Device>();
            return query;
        }
    }
}
