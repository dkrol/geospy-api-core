﻿using GS.Db.DataBase;
using GS.Db.Factory;
using GS.Domain.Device;
using GS.Query.HandleQuery;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace GS.Query.GetDevices
{
    public class GetDevicesByUserNameHandler : IHandleQuery<GetDevicesByUserNameQuery>
    {
        private readonly IMongoCollection<Device> _deviceColl;

        public GetDevicesByUserNameHandler()
        {
            _deviceColl = MongoFactory.GetCollection<Device>(DataConst.GsDevice);
        }

        public GetDevicesByUserNameQuery Execute(GetDevicesByUserNameQuery query)
        {
            query.Devices = _deviceColl.AsQueryable<Device>().Where(dev => dev.UserName == query.UserName);
            return query;
        }
    }
}
