﻿using System.Linq;
using GS.Domain.User;
using GS.Query.Query;

namespace GS.Query.GetUsers
{
    public class GetUsersQuery : IQuery
    {
        public IQueryable<UserResponse> UsersResponse { get; set; }
    }
}
