﻿using GS.Domain.User;
using GS.Query.Query;

namespace GS.Query.GetUsers
{
    public class GetUserByIdQuery : IQuery
    {
        public string UserId { get; set; }
        public User User { get; set; }
        public UserResponse UserResponse { get; set; }
    }
}
