﻿using System.Collections.Generic;
using System.Linq;
using GS.Db.DataBase;
using GS.Db.Factory;
using GS.Domain.Device;
using GS.Domain.Role;
using GS.Domain.User;
using GS.Query.HandleQuery;
using MongoDB.Driver;

namespace GS.Query.GetUsers
{
    public class GetUsersHandler : IHandleQuery<GetUsersQuery>
    {
        private readonly IMongoCollection<User> _userColl;
        private readonly IMongoCollection<Device> _deviceColl;
        private readonly IMongoCollection<Role> _roleColl;

        public GetUsersHandler()
        {
            _userColl = MongoFactory.GetCollection<User>(DataConst.GsUser);
            _roleColl = MongoFactory.GetCollection<Role>(DataConst.GsRole);
            _deviceColl = MongoFactory.GetCollection<Device>(DataConst.GsDevice);
        }

        public GetUsersQuery Execute(GetUsersQuery query)
        {
            var usersResponse = new List<UserResponse>();
            foreach (var user in _userColl.AsQueryable<User>())
            {
                var role = GetUserRoleByUserId(user.Id);
                var roleResponse = new RoleResponse()
                {
                    Id = role.Id,
                    UserId = role.UserId,
                    Name = role.Name.ToString()
                };

                var userResponse = new UserResponse
                {
                    Id = user.Id,
                    UserName = user.UserName,
                    Devices = GetUserDevicesByUserName(user.UserName),
                    Role = roleResponse
                };

                usersResponse.Add(userResponse);
            }

            query.UsersResponse = usersResponse.AsQueryable();

            return query;
        }

        private IQueryable<Device> GetUserDevicesByUserName(string userName)
        {
            return _deviceColl.AsQueryable<Device>().Where(dev => dev.UserName == userName);
        }

        private Role GetUserRoleByUserId(string userId)
        {
            return _roleColl.Find<Role>(role => role.UserId == userId).FirstOrDefault();
        }
    }
}
