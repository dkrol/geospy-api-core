﻿using GS.Domain.User;
using GS.Query.Query;

namespace GS.Query.GetUsers
{
    public class GetUserByNameQuery : IQuery
    {
        public string UserName { get; set; }
        public User User { get; set; }
        public UserResponse UserResponse { get; set; }
    }
}
