﻿using GS.Db.DataBase;
using GS.Db.Factory;
using GS.Domain.Role;
using GS.Domain.User;
using GS.Query.HandleQuery;
using MongoDB.Driver;

namespace GS.Query.GetUsers
{
    public class GetUserByNameHandler : IHandleQuery<GetUserByNameQuery>
    {
        private readonly IMongoCollection<User> _userColl;
        private readonly IMongoCollection<Role> _roleColl;

        public GetUserByNameHandler()
        {
            _userColl = MongoFactory.GetCollection<User>(DataConst.GsUser);
            _roleColl = MongoFactory.GetCollection<Role>(DataConst.GsRole);
        }

        public GetUserByNameQuery Execute(GetUserByNameQuery query)
        {
            var user = _userColl.Find<User>(usr => usr.UserName == query.UserName).FirstOrDefault();

            if (user != null)
            {
                query.User = user;

                var role = GetUserRoleByUserId(user.Id);
                var roleResponse = new RoleResponse()
                {
                    Id = role.Id,
                    UserId = role.UserId,
                    Name = role.Name.ToString()
                };

                query.UserResponse = new UserResponse()
                {
                    Id = user.Id,
                    UserName = user.UserName,
                    Role = roleResponse
                };
            }

            return query;
        }

        private Role GetUserRoleByUserId(string userId)
        {
            return _roleColl.Find<Role>(role => role.UserId == userId).FirstOrDefault();
        }
    }
}
