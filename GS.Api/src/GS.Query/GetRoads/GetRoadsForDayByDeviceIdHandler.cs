﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using GS.Db.DataBase;
using GS.Db.Factory;
using GS.Domain.Location;
using GS.Domain.Response;
using GS.Domain.Road;
using GS.Domain.RoadPath;
using GS.Domain.RoadPoint;
using GS.Query.HandleQuery;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using Newtonsoft.Json;

namespace GS.Query.GetRoads
{
    public class GetRoadsForDayByDeviceIdHandler : IHandleQuery<GetRoadsForDayByDeviceIdQuery>
    {
        private readonly IMongoCollection<Road> _roadColl;
        private readonly IMongoCollection<Location> _locationColl;
        private readonly IMongoCollection<RoadPath> _roadPathColl;
        private readonly IMongoCollection<RoadPoint> _roadPointColl;

        const int HourLimit = 10000;
        const int LocationLimit = 96;

        public GetRoadsForDayByDeviceIdHandler()
        {
            _locationColl = MongoFactory.GetCollection<Location>(DataConst.GsLocation);
            _roadColl = MongoFactory.GetCollection<Road>(DataConst.GsRoad);
            _roadPathColl = MongoFactory.GetCollection<RoadPath>(DataConst.GsRoadPath);
            _roadPointColl = MongoFactory.GetCollection<RoadPoint>(DataConst.GsRoadPoint);
        }

        public GetRoadsForDayByDeviceIdQuery Execute(GetRoadsForDayByDeviceIdQuery query)
        {
            var dateNow = long.Parse(DateTime.Now.ToString("yyyyMMdd"));
            var road = _roadColl.Find<Road>(rd => rd.DeviceId == query.DeviceId && rd.Date == query.Date).FirstOrDefault();
            var isRoadUnsaved = road == null;

            if (road == null)
            {
                var locations = _locationColl.AsQueryable<Location>()
                    .Where(loc => 
                        loc.DeviceId == query.DeviceId && 
                        loc.Timestamp >= query.DateFrom && 
                        loc.Timestamp < query.DateTo)
                    .OrderBy(loc => loc.Timestamp)
                    .ToList();

                isRoadUnsaved = locations.Count > 0;

                if (locations.Count > 0)
                {
                    var locationsGroup = PrepareLocationsGroup(locations);
                    List<List<Location>> locationsGroupResult;
                    query.Road.RoadPaths = GetRoadPathFromLocations(locationsGroup, out locationsGroupResult);
                    query.Road.RoadLocations = locationsGroupResult;
                }
            }
            else
            {
                query.Road = GetRoadFromDbWithAllRef(query, road);
            }

            if (query.Date < dateNow && isRoadUnsaved)
            {
                SaveRoad(query.Road);
            }

            return query;
        }

        private Road GetRoadFromDbWithAllRef(GetRoadsForDayByDeviceIdQuery query, Road road)
        {
            var locations = _locationColl.AsQueryable<Location>()
                    .Where(loc => loc.Timestamp >= query.DateFrom && loc.Timestamp < query.DateTo).OrderBy(loc => loc.Timestamp).ToList();

            road.RoadLocations = PrepareLocationsGroup(locations);
            road.RoadLocations = CheckAndPrepareLocationsGroup(road.RoadLocations);

            road.RoadPaths =
                _roadPathColl.Find<RoadPath>(roadPath => roadPath.RoadId == road.Id).ToList();

            road.RoadPaths.ForEach(roadPath =>
            {
                roadPath.RoadPoints =
                    _roadPointColl.Find<RoadPoint>(roadPoint => roadPoint.RoadPathId == roadPath.Id).ToList();
            });

            return road;
        }

        private void SaveRoad(Road road)
        {
            _roadColl.InsertOne(road);

            road.RoadPaths.ForEach(roadPath => roadPath.RoadId = road.Id);

            _roadPathColl.InsertMany(road.RoadPaths);

            road.RoadPaths.ForEach(roadPath =>
            {
                roadPath.RoadPoints.ForEach(roadPoint => roadPoint.RoadPathId = roadPath.Id);
                _roadPointColl.InsertMany(roadPath.RoadPoints);
            });
        }

        private List<RoadPath> GetRoadPathFromLocations(List<List<Location>> locationsGroup, out List<List<Location>> locationsGroupResult)
        {
            var roadPaths = new List<RoadPath>();
            locationsGroupResult = locationsGroup;

            foreach (var locationsPart in locationsGroup)
            {
                using (HttpClient client = new HttpClient())
                {
                    var path = GetLocationsPath(locationsPart);
                    var url = GetSnapToRoadUrl(path);
                    var response = client.GetAsync(url).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var responseContent = response.Content;
                        string responseString = responseContent.ReadAsStringAsync().Result;

                        var roadPath = new RoadPath()
                        {
                            DateFrom = locationsPart[0].Timestamp,
                            DateTo = locationsPart[locationsPart.Count - 1].Timestamp,
                            RoadPoints = GetRoadPointsFromResponseString(responseString)
                        };

                        roadPaths.Add(roadPath);
                    }
                }
            }

            if (roadPaths.Count > 1)
            {
                roadPaths = CheckAndPrepareRoadPaths(roadPaths);
                locationsGroupResult = CheckAndPrepareLocationsGroup(locationsGroup);
            }

            return roadPaths;
        }

        private List<RoadPath> CheckAndPrepareRoadPaths(List<RoadPath> roadPaths)
        {
            List<RoadPath> result = new List<RoadPath>();
            for (var i = 1; i < roadPaths.Count; i++)
            {
                var roadPath = roadPaths[i - 1];
                var roadPathNext = roadPaths[i];

                if (roadPathNext.DateFrom - roadPath.DateTo < HourLimit)
                {
                    var roadPoints = roadPath.RoadPoints;
                    roadPoints.AddRange(roadPathNext.RoadPoints);

                    roadPathNext.RoadPoints = roadPoints;
                    roadPathNext.DateFrom = roadPath.DateFrom;

                    if (i == roadPaths.Count - 1)
                    {
                        result.Add(roadPathNext);
                    }
                }
                else if (i == roadPaths.Count - 1)
                {
                    result.Add(roadPath);
                    result.Add(roadPathNext);
                }
                else
                {
                    result.Add(roadPath);
                }
            }

            return result;
        }

        private List<List<Location>> CheckAndPrepareLocationsGroup(List<List<Location>> locationsGroup)
        {
            List<List<Location>> result = new List<List<Location>>();
            for (var i = 1; i < locationsGroup.Count; i++)
            {
                var locations = locationsGroup[i - 1];
                var locationsNext = locationsGroup[i];

                if (locationsNext[0].Timestamp - locations[locations.Count - 1].Timestamp < HourLimit)
                {
                    var locationsTmp = locations;
                    locationsTmp.AddRange(locationsNext);
                    locationsGroup[i] = locationsTmp;

                    if (i == locationsGroup.Count - 1)
                    {
                        result.Add(locationsGroup[i]);
                    }
                }
                else if (i == locationsGroup.Count - 1)
                {
                    result.Add(locations);
                    result.Add(locationsNext);
                }
                else
                {
                    result.Add(locations);
                }
            }

            return result;
        }

        private List<RoadPoint> GetRoadPointsFromResponseString(string responseString)
        {
            var result = new List<RoadPoint>();
            var snappedPointsResponse =
                (SnappedPointsResponse) JsonConvert.DeserializeObject(responseString, typeof(SnappedPointsResponse));

            foreach (var snappedPoint in snappedPointsResponse.SnappedPoints)
            {
                result.Add(new RoadPoint()
                {
                    Latitude = snappedPoint.Location.Latitude,
                    Longitude = snappedPoint.Location.Longitude,
                    PlaceId = snappedPoint.PlaceId
                });
            }

            return result;
        }

        private List<List<Location>> PrepareLocationsGroup(List<Location> locations)
        {
            var locationsGroup = new List<List<Location>>();
            var locationsPart = new List<Location>();

            for (var i = 1; i < locations.Count; i++)
            {
                var loc = locations[i - 1];
                var locNext = locations[i];

                locationsPart.Add(loc);

                var diffTime = Math.Abs(locNext.Timestamp - loc.Timestamp);

                if (diffTime > HourLimit || locationsPart.Count > LocationLimit)
                {
                    locationsGroup.Add(locationsPart);
                    locationsPart = new List<Location>();
                }

                if (i == locations.Count - 1)
                {
                    locationsPart.Add(locNext);
                    locationsGroup.Add(locationsPart);
                    locationsPart = new List<Location>();
                }
            }

            return locationsGroup;
        }

        private string GetLocationsPath(List<Location> locations)
        {
            string result = string.Empty;

            for (var i = 0; i < locations.Count; i++)
            {
                var loc = locations[i];
                result += loc.Latitude + ',' + loc.Longitude;
                result += i == locations.Count - 1 ? string.Empty : "|";
            }

            return result;
        }

        private string GetSnapToRoadUrl(string path)
        {
            var key = "AIzaSyAJqThp7W9FotiFLJDUPQPYaiWu9ZSuv2s";
            var url = "https://roads.googleapis.com/v1/snapToRoads?path=";
            return url + path + "&interpolate=true&key=" + key;
        }
    }
}
