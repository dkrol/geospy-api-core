﻿using GS.Domain.Road;
using GS.Query.Query;
using System;

namespace GS.Query.GetRoads
{
    public class GetRoadsForDayByDeviceIdQuery : BaseQuery
    {
        public string DeviceId { get; set; }

        public long Date { get; set; }

        public Road Road { get; set; }

        public long DateFrom => Date * 1000000;

        public long DateTo => long.Parse(DateFormat.AddDays(1).ToString("yyyyMMdd")) * 1000000;

        private DateTime DateFormat => DateTime.ParseExact(this.Date.ToString(), "yyyyMMdd",
            System.Globalization.CultureInfo.InvariantCulture);
    }
}
