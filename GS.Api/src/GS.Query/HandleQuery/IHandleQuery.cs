﻿using GS.Query.Query;

namespace GS.Query.HandleQuery
{
    public interface IHandleQuery
    {
    }

    public interface IHandleQuery<TQuery> : IHandleQuery where TQuery : IQuery
    {
        TQuery Execute(TQuery query);
    }
}
