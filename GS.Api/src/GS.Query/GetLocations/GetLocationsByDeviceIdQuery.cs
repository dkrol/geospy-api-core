﻿using System.Linq;
using GS.Domain.Location;
using GS.Query.Query;

namespace GS.Query.GetLocations
{
    public class GetLocationsByDeviceIdQuery : IQuery
    {
        public string DeviceId { get; set; }
        public IQueryable<Location> Locations { get; set; }
    }
}
