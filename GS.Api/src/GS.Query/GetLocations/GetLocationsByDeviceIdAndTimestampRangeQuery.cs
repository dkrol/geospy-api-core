﻿using System;
using System.Linq;
using GS.Domain.Location;
using GS.Query.Query;

namespace GS.Query.GetLocations
{
    public class GetLocationsByDeviceIdAndTimestampRangeQuery : IQuery
    {
        public string DeviceId { get; set; }
        public long From { get; set; }
        public long To { get; set; }
        public IQueryable<Location> Locations { get; set; }
    }
}
