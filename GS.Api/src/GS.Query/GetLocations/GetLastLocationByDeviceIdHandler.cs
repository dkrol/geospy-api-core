﻿using System.Linq;
using GS.Db.DataBase;
using GS.Db.Factory;
using GS.Domain.Location;
using GS.Query.HandleQuery;
using MongoDB.Driver;

namespace GS.Query.GetLocations
{
    public class GetLastLocationByDeviceIdHandler : IHandleQuery<GetLastLocationByDeviceIdQuery>
    {
        private readonly IMongoCollection<Location> _locationColl;

        public GetLastLocationByDeviceIdHandler()
        {
            _locationColl = MongoFactory.GetCollection<Location>(DataConst.GsLocation);
        }
        public GetLastLocationByDeviceIdQuery Execute(GetLastLocationByDeviceIdQuery query)
        {
            query.Location = _locationColl.AsQueryable<Location>().Where(loc => loc.DeviceId == query.DeviceId).OrderByDescending(loc => loc.Timestamp).FirstOrDefault();
            return query;
        }
    }
}
