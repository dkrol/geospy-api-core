﻿using System.Linq;
using GS.Db.DataBase;
using GS.Db.Factory;
using GS.Domain.Location;
using GS.Query.HandleQuery;
using MongoDB.Driver;

namespace GS.Query.GetLocations
{
    public class GetLocationsByDeviceIdAndTimestampRangeHandler : IHandleQuery<GetLocationsByDeviceIdAndTimestampRangeQuery>
    {
        private readonly IMongoCollection<Location> _locationColl;

        public GetLocationsByDeviceIdAndTimestampRangeHandler()
        {
            _locationColl = MongoFactory.GetCollection<Location>(DataConst.GsLocation);
        }

        public GetLocationsByDeviceIdAndTimestampRangeQuery Execute(GetLocationsByDeviceIdAndTimestampRangeQuery query)
        {
            query.Locations = _locationColl.AsQueryable<Location>()
                .Where(loc => loc.DeviceId == query.DeviceId && loc.Timestamp >= query.From && loc.Timestamp <= query.To)
                .OrderBy(loc => loc.Timestamp);

            return query;
        }
    }
}
