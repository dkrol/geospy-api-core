﻿using System.Linq;
using GS.Domain.Location;
using GS.Query.Query;

namespace GS.Query.GetLocations
{
    public class GetLocationsQuery : IQuery
    {
        public IQueryable<Location> Locations { get; set; }
    }
}
