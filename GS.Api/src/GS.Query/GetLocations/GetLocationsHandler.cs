﻿using System.Linq;
using GS.Db.DataBase;
using GS.Db.Factory;
using GS.Domain.Location;
using GS.Query.HandleQuery;
using MongoDB.Driver;

namespace GS.Query.GetLocations
{
    public class GetLocationsHandler : IHandleQuery<GetLocationsQuery>
    {
        private readonly IMongoCollection<Location> _locationColl;

        public GetLocationsHandler()
        {
            _locationColl = MongoFactory.GetCollection<Location>(DataConst.GsLocation);
        }

        public GetLocationsQuery Execute(GetLocationsQuery query)
        {
            //TODO Pagination ! Skip(x).Take(y)
            query.Locations = _locationColl.AsQueryable<Location>().Take(10);
            return query;
        }
    }
}
