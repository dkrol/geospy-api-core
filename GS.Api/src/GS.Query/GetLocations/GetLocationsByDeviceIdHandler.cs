﻿using System.Linq;
using GS.Db.DataBase;
using GS.Db.Factory;
using GS.Domain.Location;
using GS.Query.HandleQuery;
using MongoDB.Driver;

namespace GS.Query.GetLocations
{
    public class GetLocationsByDeviceIdHandler : IHandleQuery<GetLocationsByDeviceIdQuery>
    {
        private readonly IMongoCollection<Location> _locationColl;

        public GetLocationsByDeviceIdHandler()
        {
            _locationColl = MongoFactory.GetCollection<Location>(DataConst.GsLocation);
        }

        public GetLocationsByDeviceIdQuery Execute(GetLocationsByDeviceIdQuery query)
        {
            query.Locations = _locationColl.AsQueryable<Location>().Where(loc => loc.DeviceId == query.DeviceId);
            return query;
        }
    }
}
