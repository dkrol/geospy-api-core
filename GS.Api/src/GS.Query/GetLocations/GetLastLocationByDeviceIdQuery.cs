﻿using GS.Domain.Location;
using GS.Query.Query;

namespace GS.Query.GetLocations
{
    public class GetLastLocationByDeviceIdQuery : IQuery
    {
        public string DeviceId { get; set; }
        public Location Location { get; set; }
    }
}
