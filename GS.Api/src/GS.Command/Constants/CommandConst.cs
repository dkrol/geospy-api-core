﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GS.Command.Constants
{
    public static class CommandConst
    {
        public const string UserNameRequired = "USERNAME_REQUIRED";
        public const string UserNameMinLength = "USERNAME_MIN_LENGTH_6";
        public const string PasswordRequired = "PASSWORD_REQUIRED";
        public const string PasswordMinLength = "PASSWORD_MIN_LENGTH_8";
        public const string ConfirmPasswordRequired = "CONFIRM_PASSWORD_REQUIRED";
        public const string ConfirmPasswordNotEqualPassword = "CONFIRM_PASSWORD_NOT_EQUAL_PASSWORD";
        public const string UserNameExist = "USERNAME_EXIST";
        public const string ImeiRequired = "IMEI_REQUIRED";
        public const string ImeiLength = "IMEI_LENGTH";
        public const string NameRequired = "NAME_REQUIRED";
        public const string LatitudeRequired = "LATITUDE_REQUIRED";
        public const string LongitudeRequired = "LONGITUDE_REQUIRED";
        public const string SpeedRequired = "SPEED_REQUIRED";
        public const string TimestampRequired = "TIMESTAMP_REQUIRED";
        public const string DeviceIdRequired = "DEVICE_ID_REQUIRED";
    }
}
