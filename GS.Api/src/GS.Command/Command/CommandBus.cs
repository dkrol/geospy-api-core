﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GS.Command.HandleCommand;

namespace GS.Command.Command
{
    public class CommandBus : ICommandBus
    {
        private readonly IHandleCommand _handleCommand;

        public CommandBus(IHandleCommand handleCommand)
        {
            _handleCommand = handleCommand;
        }

        public T SendCommand<T>(T command) where T : ICommand
        {
            return ((IHandleCommand<T>)_handleCommand).Handle(command);
        }
    }
}
