﻿namespace GS.Command.Command
{
    public interface ICommandBus
    {
        T SendCommand<T>(T cmd) where T : ICommand;
    }
}
