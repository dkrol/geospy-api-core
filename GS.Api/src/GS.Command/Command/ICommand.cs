﻿namespace GS.Command.Command
{
    public interface ICommand
    {
        bool IsValid { get; set; }
        string Message { get; set; }
    }
}
