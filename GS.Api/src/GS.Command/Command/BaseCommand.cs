﻿using MongoDB.Bson.Serialization.Attributes;

namespace GS.Command.Command
{
    public class BaseCommand : ICommand
    {
        [BsonIgnore]
        public bool IsValid { get; set; } = true;
        [BsonIgnore]
        public string Message { get; set; }
    }
}
