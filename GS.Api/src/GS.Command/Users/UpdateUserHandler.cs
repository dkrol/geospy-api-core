﻿using System;
using GS.Command.HandleCommand;
using GS.Db.DataBase;
using GS.Db.Factory;
using GS.Domain.Role;
using GS.Domain.User;
using MongoDB.Driver;

namespace GS.Command.Users
{
    public class UpdateUserHandler : IHandleCommand<UpdateUserCommand>
    {
        private readonly IMongoCollection<User> _userColl;
        private readonly IMongoCollection<Role> _roleColl;

        public UpdateUserHandler()
        {
            _userColl = MongoFactory.GetCollection<User>(DataConst.GsUser);
            _roleColl = MongoFactory.GetCollection<Role>(DataConst.GsRole);
        }

        public UpdateUserCommand Handle(UpdateUserCommand command)
        {
            User userFromDb = _userColl.Find<User>(usr => usr.Id == command.UserId).FirstOrDefault();

            if (userFromDb != null)
            {
                if (!string.IsNullOrEmpty(command.RoleName))
                {
                    UpdateUserRole(command.UserId, command.RoleName);
                }
            }

            return command;
        }

        private void UpdateUserRole(string userId, string roleName)
        {
            RoleName roleNameParseResult;
            if (Enum.TryParse(roleName, true, out roleNameParseResult))
            {
                var filter = Builders<Role>.Filter.Eq(role => role.UserId, userId);
                var update = Builders<Role>.Update.Set(role => role.Name, roleNameParseResult);
                _roleColl.UpdateOne(filter, update);
            }
        }
    }
}
