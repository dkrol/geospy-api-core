﻿using GS.Command.Command;

namespace GS.Command.Users
{
    public class DeleteUserByIdCommand : BaseCommand
    {
        public string UserId { get; set; }
    }
}
