﻿using System.ComponentModel.DataAnnotations;
using GS.Command.Command;
using GS.Command.Constants;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace GS.Command.Users
{
    public class AddUserCommand : BaseCommand
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [Required(ErrorMessage = CommandConst.UserNameRequired)]
        [StringLength(100, MinimumLength = 6, ErrorMessage = CommandConst.UserNameMinLength)]
        public string UserName { get; set; }

        [Required(ErrorMessage = CommandConst.PasswordRequired)]
        [StringLength(100, MinimumLength = 8, ErrorMessage = CommandConst.PasswordMinLength)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = CommandConst.PasswordRequired)]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = CommandConst.ConfirmPasswordNotEqualPassword)]
        [BsonIgnore]
        public string ConfirmPassword { get; set; }

        public string Sale { get; set; }
    }
}
