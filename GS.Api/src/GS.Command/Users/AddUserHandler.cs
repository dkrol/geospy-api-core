﻿using GS.Command.Constants;
using GS.Command.HandleCommand;
using GS.Db.DataBase;
using GS.Db.Factory;
using GS.Domain.Role;
using MongoDB.Driver;

namespace GS.Command.Users
{
    public class AddUserHandler : IHandleCommand<AddUserCommand>
    {
        private readonly IMongoCollection<AddUserCommand> _userColl;
        private readonly IMongoCollection<Role> _roleColl;

        public AddUserHandler()
        {
            _userColl = MongoFactory.GetCollection<AddUserCommand>(DataConst.GsUser);
            _roleColl = MongoFactory.GetCollection<Role>(DataConst.GsRole);
        }

        public AddUserCommand Handle(AddUserCommand userToAdd)
        {
            
            AddUserCommand userDb = _userColl.Find<AddUserCommand>(u => u.UserName == userToAdd.UserName).FirstOrDefault();

            if (userDb == null)
            {
                _userColl.InsertOne(userToAdd);
                AddDefaultUserRole(userToAdd.Id);
            }
            else
            {
                userToAdd.IsValid = false;
                userToAdd.Message = CommandConst.UserNameExist;
            }

            userToAdd.Password = null;
            userToAdd.ConfirmPassword = null;
            userToAdd.Sale = null;
            return userToAdd;
        }

        private void AddDefaultUserRole(string userId)
        {
            var defaultRole = new Role()
            {
                Name = RoleName.User,
                UserId = userId
            };

            _roleColl.InsertOne(defaultRole);
        }
    }
}
