﻿using System;
using GS.Command.Command;
using GS.Domain.Role;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace GS.Command.Users
{
    public class UpdateUserCommand : BaseCommand
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string UserId { get; set; }
        public string RoleName { get; set; }
    }
}
