﻿using System.Collections.Generic;
using GS.Command.HandleCommand;
using GS.Db.DataBase;
using GS.Db.Factory;
using GS.Domain.Device;
using GS.Domain.Location;
using GS.Domain.Road;
using GS.Domain.RoadPath;
using GS.Domain.RoadPoint;
using GS.Domain.Role;
using GS.Domain.User;
using MongoDB.Driver;

namespace GS.Command.Users
{
    public class DeleteUserByIdHandler : IHandleCommand<DeleteUserByIdCommand>
    {
        private readonly IMongoCollection<User> _userColl;
        private readonly IMongoCollection<Role> _roleColl;
        private readonly IMongoCollection<Device> _deviceColl;
        private readonly IMongoCollection<Location> _locationColl;
        private readonly IMongoCollection<Road> _roadColl;
        private readonly IMongoCollection<RoadPath> _roadPathColl;
        private readonly IMongoCollection<RoadPoint> _roadPointColl;

        public DeleteUserByIdHandler()
        {
            _userColl = MongoFactory.GetCollection<User>(DataConst.GsUser);
            _roleColl = MongoFactory.GetCollection<Role>(DataConst.GsRole);
            _deviceColl = MongoFactory.GetCollection<Device>(DataConst.GsDevice);
            _locationColl = MongoFactory.GetCollection<Location>(DataConst.GsLocation);
            _roadColl = MongoFactory.GetCollection<Road>(DataConst.GsRoad);
            _roadPathColl = MongoFactory.GetCollection<RoadPath>(DataConst.GsRoadPath);
            _roadPointColl = MongoFactory.GetCollection<RoadPoint>(DataConst.GsRoadPoint);
        }
        public DeleteUserByIdCommand Handle(DeleteUserByIdCommand command)
        {
            User user = _userColl.Find<User>(usr => usr.Id == command.UserId).FirstOrDefault();
            List<Device> devices = _deviceColl.Find<Device>(dev => dev.UserName == user.UserName).ToList();

            List<List<Location>> locationsGroup = new List<List<Location>>();
            List<List<Road>> roadsGroup = new List<List<Road>>();

            if (devices != null && devices.Count > 0)
            {
                foreach (var device in devices)
                {
                    locationsGroup.Add(GetLocationsByDeviceId(device.Id));
                    roadsGroup.Add(GetRoadsByDeviceIdWithAllRef(device.Id));
                }
            }

            // TODO: Delete all finded objects
            DeleteRoleByUserId(user.Id);

            return command;
        }

        private List<Location> GetLocationsByDeviceId(string deviceId)
        {
            return _locationColl.Find<Location>(location => location.DeviceId == deviceId).ToList();
        }

        private List<Road> GetRoadsByDeviceIdWithAllRef(string deviceId)
        {
            List<Road> roads = new List<Road>();
            List<Road> roadsFromDb = _roadColl.Find<Road>(road => road.DeviceId == deviceId).ToList();

            foreach (var road in roadsFromDb)
            {
                FillRoadReferences(road);
                roads.Add(road);
            }
            
            return roads;
        }

        private void FillRoadReferences(Road road)
        {
            road.RoadPaths = _roadPathColl.Find<RoadPath>(roadPath => roadPath.RoadId == road.Id).ToList();
            road.RoadPaths.ForEach(roadPath =>
            {
                roadPath.RoadPoints = _roadPointColl.Find<RoadPoint>(roadPoint => roadPoint.RoadPathId == roadPath.Id).ToList();
            });
        }

        private void DeleteRoleByUserId(string userId)
        {
            Role role = _roleColl.Find<Role>(r => r.UserId == userId).FirstOrDefault();
            //_roleColl.DeleteOne(Query.EQ(DataConst.GsId, role.Id));
        }
    }
}
