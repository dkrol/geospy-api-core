﻿using System.ComponentModel.DataAnnotations;
using GS.Command.Command;
using GS.Command.Constants;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace GS.Command.Locations
{
    public class AddLocationCommand : BaseCommand
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [Required(ErrorMessage = CommandConst.LatitudeRequired)]
        public string Latitude { get; set; }

        [Required(ErrorMessage = CommandConst.LongitudeRequired)]
        public string Longitude { get; set; }

        [Required(ErrorMessage = CommandConst.SpeedRequired)]
        public double Speed { get; set; }

        [Required(ErrorMessage = CommandConst.TimestampRequired)]
        public long Timestamp { get; set; }

        [Required(ErrorMessage = CommandConst.DeviceIdRequired)]
        [BsonRepresentation(BsonType.ObjectId)]
        public string DeviceId { get; set; }
    }
}
