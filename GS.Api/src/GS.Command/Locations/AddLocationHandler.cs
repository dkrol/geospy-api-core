﻿using GS.Command.HandleCommand;
using GS.Db.DataBase;
using GS.Db.Factory;
using GS.Domain.Location;
using MongoDB.Driver;

namespace GS.Command.Locations
{
    public class AddLocationHandler : IHandleCommand<AddLocationCommand>
    {
        private readonly IMongoCollection<AddLocationCommand> _locationColl;
        public AddLocationHandler()
        {
            _locationColl = MongoFactory.GetCollection<AddLocationCommand>(DataConst.GsLocation);
        }

        public AddLocationCommand Handle(AddLocationCommand locationToAdd)
        {
            _locationColl.InsertOne(locationToAdd);
            return locationToAdd;
        }
    }
}
