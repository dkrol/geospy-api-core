﻿using System;
using GS.Command.Command;

namespace GS.Command.HandleCommand
{
    public interface IHandleCommand
    {
    }

    public interface IHandleCommand<TCommand> : IHandleCommand where TCommand : ICommand
    {
        TCommand Handle(TCommand command);
    }
}
