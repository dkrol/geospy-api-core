﻿using GS.Command.HandleCommand;
using GS.Db.DataBase;
using GS.Db.Factory;
using GS.Domain.User;
using MongoDB.Driver;

namespace GS.Command.Devices
{
    public class AddDeviceHandler : IHandleCommand<AddDeviceCommand>
    {
        private readonly IMongoCollection<User> _userColl;
        private readonly IMongoCollection<AddDeviceCommand> _deviceColl;

        public AddDeviceHandler()
        {
            _userColl = MongoFactory.GetCollection<User>(DataConst.GsUser);
            _deviceColl = MongoFactory.GetCollection<AddDeviceCommand>(DataConst.GsDevice);
        }

        public AddDeviceCommand Handle(AddDeviceCommand deviceToAdd)
        {
            User userFromDb = _userColl.Find<User>(u => u.UserName == deviceToAdd.UserName).FirstOrDefault();

            if (userFromDb != null)
            {
                AddDeviceCommand deviceFromDb =
                    _deviceColl.Find<AddDeviceCommand>(dev => dev.UserName == deviceToAdd.UserName && dev.Imei == deviceToAdd.Imei).FirstOrDefault();

                if (deviceFromDb != null)
                {
                    deviceToAdd.Id = deviceFromDb.Id;
                    if (deviceToAdd.Name != deviceFromDb.Name)
                    {
                        var filter = Builders<AddDeviceCommand>.Filter.Eq(dev => dev.Id, deviceFromDb.Id);
                        var mongoResult = _deviceColl.ReplaceOne(filter, deviceToAdd);
                    }
                }
                else
                {
                    _deviceColl.InsertOne(deviceToAdd);
                }
            }
            else
            {
                deviceToAdd.IsValid = false;
                deviceToAdd.Message = "Username not exist in database";
            }
            
            return deviceToAdd;
        }
    }
}
