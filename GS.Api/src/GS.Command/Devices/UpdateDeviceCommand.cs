﻿using GS.Command.Command;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace GS.Command.Devices
{
    public class UpdateDeviceCommand : BaseCommand
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Imei { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Description { get; set; }
    }
}
