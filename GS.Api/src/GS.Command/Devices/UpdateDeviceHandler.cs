﻿using GS.Command.HandleCommand;
using GS.Db.DataBase;
using GS.Db.Factory;
using MongoDB.Driver;

namespace GS.Command.Devices
{
    public class UpdateDeviceHandler : IHandleCommand<UpdateDeviceCommand>
    {
        private readonly IMongoCollection<UpdateDeviceCommand> _deviceColl;
        public UpdateDeviceHandler()
        {
            _deviceColl = MongoFactory.GetCollection<UpdateDeviceCommand>(DataConst.GsDevice);
        }

        public UpdateDeviceCommand Handle(UpdateDeviceCommand updateDevice)
        {
            UpdateDeviceCommand deviceFromDb =
                _deviceColl.Find<UpdateDeviceCommand>(dev => dev.Id == updateDevice.Id).FirstOrDefault();

            if (deviceFromDb != null)
            {
                var filter = Builders<UpdateDeviceCommand>.Filter.Eq(dev => dev.Id, deviceFromDb.Id);
                var update = Builders<UpdateDeviceCommand>.Update.Set(dev => dev.Description, updateDevice.Description);
                _deviceColl.UpdateOne(filter, update);
                deviceFromDb.Description = updateDevice.Description;
            }

            return deviceFromDb;
        }
    }
}
