﻿using System.ComponentModel.DataAnnotations;
using GS.Command.Command;
using GS.Command.Constants;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace GS.Command.Devices
{
    public class AddDeviceCommand : BaseCommand
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [Required(ErrorMessage = CommandConst.ImeiRequired)]
        [StringLength(17, MinimumLength = 15, ErrorMessage = CommandConst.ImeiLength)]
        public string Imei { get; set; }

        [Required(ErrorMessage = CommandConst.NameRequired)]
        public string Name { get; set; }

        [Required(ErrorMessage = CommandConst.UserNameRequired)]
        public string UserName { get; set; }
        
        public string Description { get; set; }
    }
}
