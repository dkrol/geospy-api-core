﻿using System.Linq;
using GS.Domain.Role;

namespace GS.Domain.User
{
    public class UserResponse
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public RoleResponse Role { get; set; }
        public IQueryable<Device.Device> Devices { get; set; }
    }
}
