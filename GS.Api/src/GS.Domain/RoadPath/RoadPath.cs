﻿using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace GS.Domain.RoadPath
{
    public class RoadPath
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public long DateFrom { get; set; }
        public long DateTo { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string RoadId { get; set; }

        [BsonIgnore]
        public List<RoadPoint.RoadPoint> RoadPoints { get; set; }
    }
}
