﻿using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace GS.Domain.Road
{
    public class Road
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public long Date { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string DeviceId { get; set; }

        [BsonIgnore]
        public List<RoadPath.RoadPath> RoadPaths { get; set; }
        [BsonIgnore]
        public List<List<Location.Location>> RoadLocations { get; set; }
    }
}
