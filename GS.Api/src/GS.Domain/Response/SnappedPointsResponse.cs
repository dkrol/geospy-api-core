﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace GS.Domain.Response
{
    public class SnappedPointsResponse
    {
        [JsonProperty("snappedPoints")]
        public List<SnappedPoint> SnappedPoints { get; set; }
    }

    public class LocationRoad
    {
        [JsonProperty("latitude")]
        public double Latitude { get; set; }

        [JsonProperty("longitude")]
        public double Longitude { get; set; }
    }

    public class SnappedPoint
    {
        [JsonProperty("location")]
        public LocationRoad Location { get; set; }

        [JsonProperty("placeId")]
        public string PlaceId { get; set; }
    }
}
