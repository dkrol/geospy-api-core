﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace GS.Domain.Location
{
    public class Location
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public double Speed { get; set; }
        public long Timestamp { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string DeviceId { get; set; }
    }
}
