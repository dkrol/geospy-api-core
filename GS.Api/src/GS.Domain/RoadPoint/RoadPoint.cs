﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace GS.Domain.RoadPoint
{
    public class RoadPoint
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string PlaceId { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string RoadPathId { get; set; }
    }
}
