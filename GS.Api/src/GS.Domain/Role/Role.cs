﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace GS.Domain.Role
{
    public class Role
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public RoleName Name { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string UserId { get; set; }
    }
}
