﻿namespace GS.Domain.Role
{
    public enum RoleName
    {
        User = 1,
        Admin = 2
    }
}
