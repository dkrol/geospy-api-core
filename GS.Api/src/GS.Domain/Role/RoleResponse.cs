﻿namespace GS.Domain.Role
{
    public class RoleResponse
    {
        private string _name;
        public string Id { get; set; }
        public string Name
        {
            get { return _name; }
            set { _name = value.ToUpper(); }
        }
        public string UserId { get; set; }
    }
}
