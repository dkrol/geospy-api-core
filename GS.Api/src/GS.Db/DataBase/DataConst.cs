﻿namespace GS.Db.DataBase
{
    public class DataConst
    {
        // Database name
        public const string Geospydb = "geospydb";
        //public const string Geospydb = "geospydb_pi";

        // Collections
        public const string GsLocation = "gs_location";
        public const string GsDevice = "gs_device";
        public const string GsUser = "gs_user";
        public const string GsRoad = "gs_road";
        public const string GsRoadPath = "gs_road_path";
        public const string GsRoadPoint = "gs_road_point";
        public const string GsRole = "gs_role";

        // Base names
        public const string GsId = "_id";
    }
}
