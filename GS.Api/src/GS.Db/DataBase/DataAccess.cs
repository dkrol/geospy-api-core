﻿using MongoDB.Driver;

namespace GS.Db.DataBase
{
    public class DataAccess : IDataAccessMongo
    {
        public MongoClient Client { get; private set; }
        public IMongoDatabase Db { get; private set; }
        public string CollectionName { get; private set; }

        public DataAccess(string collectionName)
        {
            Client = new MongoClient("mongodb://localhost:27017");
            Db = Client.GetDatabase(DataConst.Geospydb);
            CollectionName = collectionName;
        }
    }
}
