﻿using MongoDB.Driver;

namespace GS.Db.DataBase
{
    public interface IDataAccessMongo
    {
        MongoClient Client { get; }
        IMongoDatabase Db { get; }
        string CollectionName { get; }
    }
}
