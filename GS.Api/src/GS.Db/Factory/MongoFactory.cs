﻿using GS.Db.DataBase;
using MongoDB.Driver;

namespace GS.Db.Factory
{
    public class MongoFactory
    {
        public static IMongoCollection<T> GetCollection<T>(string collectionName) where T : class 
        {
            IDataAccessMongo dataAccess = new DataAccess(collectionName);
            return dataAccess.Db.GetCollection<T>(dataAccess.CollectionName);
        }
    }
}
